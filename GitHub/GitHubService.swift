//
//  GitHubService.swift
//  GitHub
//
//  Created by Paulo Silva on 21/01/17.
//  Copyright © 2016 Paulo Silva. All rights reserved.
//

import UIKit
import SwiftyJSON

class GitHubService: NSObject {
    
    public func getListaRepositorio(number: Int,handlerJsonResult:@escaping (JSON?) -> ()) {
        let urlJSON : String = GitHubServicesRepository.getListaRepositorioJavaPath(number: number)
        GitHubConnection.makeHTTPGetRequest(urlJSON, onCompletion: { (jsonResult, error) -> Void in
            handlerJsonResult(jsonResult)
        })
    }
    
    public func getPullRepositorioService(loginUsuario: String, nomeRepositorio: String, handlerJsonResult:@escaping (JSON?) -> ()) {
        
        let urlJSON : String = GitHubServicesRepository.getPullRepositorioJavaPath(login: loginUsuario, nomeRepositorio: nomeRepositorio)
        
        
        GitHubConnection.makeHTTPGetRequest(urlJSON, onCompletion: { (jsonResult, error) -> Void in
            handlerJsonResult(jsonResult)
        })
    }

}
