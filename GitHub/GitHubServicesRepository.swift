//
//
//  GitHubServicesRepository.swift
//  GitHub
//
//  Created by Paulo Silva on 21/01/17.
//  Copyright © 2016 Paulo Silva. All rights reserved.
//

import Foundation
import UIKit

var pathServices = Bundle.main.path(forResource: "services", ofType: "plist")
var newHostIp : String?

class GitHubServicesRepository  {
    
    
    static func setNewHostIp(_ ip: String) {
        newHostIp = ip
    }
    
    static func getListaRepositorioJavaPath(number: Int) -> String {
        var serviceDict: NSDictionary?
        var url = "http://"
        
        if pathServices != nil {
            serviceDict = NSDictionary(contentsOfFile: pathServices!)
            var newUrl = ""
            if let ip = newHostIp {
                newUrl = ip
            } else {
                newUrl = serviceDict?.object(forKey: "ipServicosMain") as! String
            }
            
            url += serviceDict?.object(forKey: "listaApiJava") as! String
            url = url.replacingOccurrences(of: "<page>", with: String(number))
        }
        return url
    }
    
    
    static func getPullRepositorioJavaPath(login: String, nomeRepositorio: String) -> String {
        var serviceDict: NSDictionary?
        var url = "http://"
        
        if pathServices != nil {
            serviceDict = NSDictionary(contentsOfFile: pathServices!)
            var newUrl = ""
            if let ip = newHostIp {
                newUrl = ip
            } else {
                newUrl = serviceDict?.object(forKey: "ipServicosMain") as! String
            }
            
            url += serviceDict?.object(forKey: "recuperaPullRepositorio") as! String
            url = url.replacingOccurrences(of: "<criador>", with: login)
            url = url.replacingOccurrences(of: "<repositorio>", with: nomeRepositorio)
        }
        return url
    }    
  }
