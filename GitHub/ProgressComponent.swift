
import Foundation
import UIKit

class ProgressComponent : NSObject {
    static let sharedInstance = ProgressComponent()
    
    var arrayProgressComponents = [KVNProgress]()
    
    var configuration = KVNProgressConfiguration()
    var progress = KVNProgress()
    var progressSucess = KVNProgress()
    
    func initProgressComponent() {
        configuration.statusColor = Color.blue()
        configuration.statusFont = UIFont(name:"HelveticaNeue-Thin", size:14.0)
        configuration.circleStrokeForegroundColor = Color.blue()
        configuration.circleStrokeBackgroundColor = Color.set(color: Color.white(), alpha: 0.3)
        configuration.circleFillBackgroundColor = Color.set(color: Color.white(), alpha: 0.1)
        configuration.backgroundTintColor = Color.white()
        configuration.successColor = Color.darkGray()
        configuration.errorColor = Color.red()
        configuration.circleSize = 50.0
        configuration.lineWidth = 1.0
        configuration.backgroundType = KVNProgressBackgroundType.blurred
        configuration.isFullScreen = true
        
        progress.setConfiguration(configuration)
        
        if !ProgressComponent.sharedInstance.arrayProgressComponents.contains(progress) {
            ProgressComponent.sharedInstance.arrayProgressComponents.append(progress)
        }
    }
    
    func showProgressWithStatus(status: String) {
        progress.show(withStatus: status)
    }
    
    func showProgressWithStatus(status: String, onView: UIView) {
        onView.endEditing(true)
        progress.show(withStatus: status, on: onView)
    }

    func showProgressWithStatus(status: String, onView: UIView, atMainView : UIView) {
        onView.endEditing(true)
        
        let viewProgress = UIView(frame: onView.frame)
        viewProgress.tag = 950
        atMainView.addSubview(viewProgress)
        configuration.backgroundType = KVNProgressBackgroundType.solid
        configuration.backgroundFillColor = Color.white()
        progress.setConfiguration(configuration)
        progress.show(withStatus: status, on: viewProgress)
    }
    
    func dismissProgress() {
        progress.dismiss()
    }
    
    func dismissProgress(mainView : UIView) {
        if let viewProgress = mainView.viewWithTag(950) {
            viewProgress.removeFromSuperview()
        }
        progress.dismiss()
    }
    
    func succeededProgressWithStatus(status: String) {
        progressSucess.showSuccess(withStatus: status)
        Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(ProgressComponent.dismissSuccessProgress), userInfo: nil, repeats: false)
    }
    
    func dismissSuccessProgress() {
        progressSucess.dismiss()
        dismissProgress()
    }

    func errorProgressWithStatus(status: String) {
        progress.showError(withStatus: status)
    }
    
    static func dismissAllProgressComponents() {
        for view in ProgressComponent.sharedInstance.arrayProgressComponents {
            view.dismiss()
        }
    }
}
