//
//  GitHubConnection.swift
//  GitHub
//
//  Created by Paulo Silva on 21/01/17.
//  Copyright © 2016 Paulo Silva. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias ServiceResponse = (_ jsonResult: JSON?, _ error: NSError?) -> Void

 class GitHubConnection{
    
        //MARK: METODOS DA CONNECTION
        static var task = URLSessionDataTask()
        
        static var session = URLSession.shared
        
        static func makeHTTPGetRequest(_ url: String, onCompletion: @escaping ServiceResponse) {
            guard let urlConta = URL(string: url)  else {
                DispatchQueue.main.async(execute: { () -> Void in
                    onCompletion(nil, nil)
                })
                return
            }
            
             Alamofire.request(urlConta).responseJSON() { dataResponse in
                switch dataResponse.result {
                case .success(let value):
                     let myReponse = JSON(value)
                    DispatchQueue.main.async(execute: { () -> Void in
                        onCompletion(myReponse, dataResponse.result.error as NSError?)
                    })
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            
        }
    
    }
