#import "UIImage+KVNEmpty.h"

@implementation UIImage (KVNEmpty)

+ (UIImage *)emptyImage
{
	return [[UIImage alloc] init];
}

@end
