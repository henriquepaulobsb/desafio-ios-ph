//
//  DetailsRepositorioJavaCell.swift
//  GitHub
//
//  Created by Paulo Silva on 21/01/17.
//  Copyright © 2016 Paulo Silva. All rights reserved.
//

import UIKit

class DetailsRepositorioJavaCell: UITableViewCell {

    
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

   
    
    
}
