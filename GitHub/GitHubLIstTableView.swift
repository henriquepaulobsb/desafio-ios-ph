//
//  GitHubLIstTableView.swift
//  GitHub
//
//  Created by Paulo Silva on 21/01/17.
//  Copyright © 2016 Paulo Silva. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class GitHubLIstTableView: UITableViewController {
  
    var gitHubListController = GitHubListController()
     var myResponse  :   JSON    =   nil
    var listaRepositorio : [GitHubClass] = []
    var itens : [Items] = []
    
    var isRefreshing : Bool = false
    var numberPage : Int = 1
    var progressComponent = ProgressComponent()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "GitHub JavaPop"
        progressComponent.initProgressComponent()
        getListRepositorioJava(number: numberPage)


    }

    func getListRepositorioJava(number: Int) {
        progressComponent.showProgressWithStatus(status: "Carregando Dados")
        self.isRefreshing = true
        gitHubListController.getListaRepositorio(number: number){ (jsonResult) in
            if jsonResult != nil{
                self.myResponse =   jsonResult!
                let singleUser   =   GitHubClass(json:self.myResponse)
                self.itens += singleUser.items!
                self.numberPage += 1
                self.isRefreshing = false
            }
            self.progressComponent.dismissProgress(mainView: self.tableView)
            self.tableView.reloadData()
        }
    }
    
    
    func reloadTableView(notification : NSNotification) {
        self.tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "identifierListRepository", for: indexPath) as! ListRepositorioJavaCell
        
        let item = self.itens[(indexPath as NSIndexPath).row]
        
        cell.nomeRepositorioLabel.text = item.name
        cell.descricaoLabel.text = item.descriptionValue
        cell.forksLabel.text = String(describing: item.forksCount!)
        cell.starLabel.text = String(describing: item.stargazersCount!)
        
//        cell.imageAvatar.sd_setImage(with: URL(string: (item.owner?.avatarUrl!)! ), placeholderImage: #imageLiteral(resourceName: "loader.gif"),options: [.continueInBackground, .progressiveDownload])
        
        getDataFromUrl(url: URL(string: (item.owner?.avatarUrl!)!)!) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                
                cell.imageAvatar.image = UIImage(data: data)!
            }
        }
        cell.userNameLabel.text = item.owner?.login
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itens.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if !self.itens.isEmpty{
            return 1
        }
            return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.itens[indexPath.row]
        self.performSegue(withIdentifier: "descriptionSegue", sender: item)
    }
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height{
            if !isRefreshing {
                getListRepositorioJava(number: numberPage+1)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "descriptionSegue" {
            
            let detailsViewController: DetailsViewController = segue.destination as! DetailsViewController
            let indexPath = self.tableView.indexPathForSelectedRow!
            
            if !itens.isEmpty{
                detailsViewController.itemSelecionado = itens[indexPath.row]
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
}
